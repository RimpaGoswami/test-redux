/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';


import EmployeeDetails from './src/screens/EmployeeDetails';
import Navigation from './src/components/Navigation'

import { PersistGate } from 'redux-persist/es/integration/react'
import { Provider } from 'react-redux';

import { store, persistor } from './src/redux/stores/Store'
export default App = () => {
return (
// Redux: Global Store
<Provider store={store}>
<PersistGate
loading={null}
persistor={persistor}
>
<Navigation/>
</PersistGate>
</Provider>
);
};

/*class App extends Component{
    render(){
      return(
        <View>
          <EmployeeDetails/>
        </View>

      )
    }

}


export default App;*/
