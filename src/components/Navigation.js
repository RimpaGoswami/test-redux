import React,{Component} from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import EmployeeDetails from '../screens/EmployeeDetails';
import ShowEmployeeDetails from '../screens/ShowEmployeeDetails'

const Stack=createStackNavigator();

const Navigation=()=>
{


    return(
    
    <NavigationContainer>
    
        <Stack.Navigator  headerMode='none'>
          <Stack.Screen name="EmployeeDetails" component={EmployeeDetails}
              options={{ title: 'Welcome' } }
            />
            <Stack.Screen name="ShowEmployeeDetails" component={ShowEmployeeDetails} />
    
            
            
            
        </Stack.Navigator>
      
      </NavigationContainer>
      )
}
export default Navigation