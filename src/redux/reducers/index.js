import employeeDetailReducer from './EmployeeDetailReducer'
import { combineReducers } from 'redux'
const rootReducer= combineReducers({
    employeeDetailReducer
})

export default rootReducer;