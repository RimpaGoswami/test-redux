import React,{Component} from "react";
import {View, Text,StyleSheet, SafeAreaView,TextInput,TouchableOpacity} from "react-native";
import {connect} from "react-redux"
import {saveEmployeeDetails} from '../redux/actions/EmployeeDetailAction'

class EmployeeDetails extends Component{
                                        constructor(props){
                                         super(props)
                                         this.state = {
                                                            name:"",
                                                            schoolName:"",
                                                            companyName:""
                                                      }
   
                                                        }




                                         render(){
                                                 return(
       
                                                            <View style={styles.mainView}>
                                                                   <Text style={styles.mainTextStyle}>Submit Employee Details</Text>
                                                                   <Text style={styles.textStyle}>Enter Your Name</Text>
                                                                    <TextInput
                                                                             style={styles.textInputStyle}
                                                                              onChangeText={name => {
                                                                             this.setState({ name: name }, () => {
                                                                          });
                                                                          }}
                                                                              value={this.state.name}
                                                                              underlineColorAndroid='rgba(0,0,0,0)' 
                                                                              placeholder="Enter Your  Name"
                                                                              placeholderTextColor = "#002f6c"
                                                                              selectionColor="#fff"
                                                                    ></TextInput>
                                                                    <Text style={styles.textStyle}>Enter Your School Name</Text>
                                                                    <TextInput
                                                                              style={styles.textInputStyle}
                                                                              underlineColorAndroid="transparent"
                                                                              placeholder="Enter Your School Name"
                                                                              placeholderTextColor = "#002f6c"
                                                                              onChangeText={schoolName => {
                                                                              this.setState({ schoolName: schoolName }, () => {
                                                                          });
                                                                          }}
                                                                              value={this.state.schoolName}
          
                                                                    />
                                                                  <Text style={styles.textStyle}>Enter Your Company Name</Text>
                                                                  <TextInput
                                                                             style={styles.textInputStyle}
                                                                             underlineColorAndroid="transparent"
                                                                             placeholderTextColor="#cccccc"
                                                                             placeholder="Enter Your Company Name"
                                                                             placeholderTextColor = "#002f6c"
                                                                             onChangeText={companyName => {
                                                                             this.setState({ companyName: companyName }, () => {
                                                                         });
                                                                         }}
                                                                             value={this.state.companyName}
                                                                      />
                                                                  <TouchableOpacity
                                                                             underlayColor="transparent"
                                                                             style={
                                                                             styles.buttonStyle
                                                                                   }
                                                                             onPress={() => {
                                                                             var employeeDetails = {};
                                                                             employeeDetails.name = this.state.name;
                                                                             employeeDetails.schoolName = this.state.schoolName;
                                                                             employeeDetails.companyName = this.state.companyName;
                                                                             this.props.reduxSaveEmployeeDetail(employeeDetails)
                                                                             this.props.navigation.navigate("ShowEmployeeDetails")
                                                                         }}
          
                                                                   >
                                                                            <Text style={styles.buttonTextStyle}>Submit</Text>
                                                                   </TouchableOpacity>
                                                            </View>
            
                                                         )
                                                     }
                                           }
        const styles = StyleSheet.create({
        container: {
            flex: 1,
            width: "100%",
            height:"100%",
            justifyContent: 'flex-start',
            alignItems: 'center',
            backgroundColor:"lightgray",
        //   paddingHorizontal:10,
            paddingBottom:50
        },
        mainView:{
            width:"100%",
            height:"50%",
            justifyContent: "flex-start",
            alignItems: "center",
            padding:20,
        },
        textInputStyle:{
            width:300,
            height:40,
            backgroundColor:"white",
            paddingHorizontal:15,
            marginBottom:10,
            marginTop:20
        },
        textStyle:{
            width:"100%",
            height:20,
        //paddingHorizontal:15,
            textAlign:"left",
            marginTop:10,
            fontSize:15
        },
      mainTextStyle:{
            width:"100%",
            height:40,
        //paddingHorizontal:15,
            textAlign:"center",
            marginTop:30,
            marginBottom:10,
            fontSize:20
        },
     buttonStyle:{
            width: "100%",
            height: 40,
            borderRadius: 5,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#F3DDB5",
            marginTop:20
            },
    buttonTextStyle:{
            width:"100%",
            height:"100%",
            textAlign:"center",
            marginTop:10,
            fontSize:18
            },
            })

            mapDispatchToProps = (dispatch) => {
                return{
                         reduxSaveEmployeeDetail:(employeDetails) => dispatch(saveEmployeeDetails(employeDetails))
                }}


                export default connect(
                    null,
                    mapDispatchToProps
                    )(EmployeeDetails);