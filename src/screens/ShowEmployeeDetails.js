import React from "react";
import {View, Text,SafeAreaView,StyleSheet,TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import {employeeDetailReducer} from '../redux/reducers/EmployeeDetailReducer'
class ShowEmployeeDetail extends React.Component{
    constructor(props){
        super(props)
    }
render(){
return(
        <View style={{marginTop:30}}>
            <Text style={styles.mainTextStyle}>Show Employee Details </Text>
            <View style={styles.textViewStyle}>
                      <Text style={styles.textStyle}>Name:   </Text>
                      <Text style={styles.textStyle}> {this.props.employeeDetails.name}</Text>
            </View>
            <View style={styles.textViewStyle}>
<                     Text style={styles.textStyle}>School Name:    </Text>
                     <Text style={styles.textStyle}>{this.props.employeeDetails.schoolName}</Text>
           </View>
           <View style={styles.textViewStyle}>
                    <Text style={styles.textStyle}>Company Name:   </Text>
                    <Text style={styles.textStyle}>{this.props.employeeDetails.companyName}</Text>
          </View>
           <TouchableOpacity
                     underlayColor="transparent"
                     style={
                     styles.buttonStyle
                      }
                     onPress={() => {
                     this.props.navigation.navigate("EmployeeDetails")
                     }}
          
                    >
                    <Text style={styles.buttonTextStyle}>Back</Text>
                </TouchableOpacity>
        </View>

)
}}
const styles = StyleSheet.create({
container: {
                flex: 1,
                width: "100%",
                height:"100%",
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                backgroundColor:"lightgray",
            },
textViewStyle:{
                flexDirection:"row",
                paddingBottom:20,
                marginHorizontal:20
              },
textStyle:{
                height:20,
                textAlign:"left",
                marginTop:10,
                fontSize:15
          },
mainTextStyle:{
                width:"100%",
                height:40,
                textAlign:"center",
                marginTop:10,
                marginBottom:10,
                fontSize:20
},
buttonTextStyle:{
                width:"100%",
                height:"100%",
                textAlign:"center",
                marginTop:10,
                fontSize:18
                },
buttonStyle:{
               width: 200,
               height: 40,
               borderRadius: 5,
               justifyContent: "center",
               alignItems: "center",
               alignSelf:'center',
               backgroundColor: "#F3DDB5",
               marginTop:20
    },
})
const mapStateToProps = (state) => {
     return         {
                                 employeeDetails: state.employeeDetailReducer.employeeDetails
                    }
            }
                    export default connect(mapStateToProps,null)(ShowEmployeeDetail)